# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users
      resources :products
      resources :tasks
    end
  end
  namespace :api, defaults: { format: "json" }  do
    namespace :v1 do
      devise_for "users",
               path: "",
               path_names: {
                 sign_in: "login",
                 sign_out: "logout",
                 registration: "signup"
               },
               controllers: {
                 sessions: "api/v1/users/sessions",
                 registrations: "api/v1/users/registrations"
               },
               singular: :user
    end
  end

  match "*unmatched", to: "application#route_not_found", via: :all
end
