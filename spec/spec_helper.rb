# frozen_string_literal: true

require "ffaker"
require "jsonapi/rspec"
require "devise/jwt/test_helpers"

Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

RSpec.configure do |config|
  config.include FactoryBot
  config.include ApiBase, type: :request

  config.include JSONAPI::RSpec

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
