# frozen_string_literal: true

module ApiBase
  [200, 201, 204, 400, 401, 403, 404, 422].each do |status_code|
    shared_examples "returns #{status_code} HTTP status" do
      it "returns #{status_code}" do
        execute unless response
        expect(response.status).to eq(status_code)
      end
    end
  end

  shared_context "API v1 authorization token" do
    let(:headers_bearer) do
      headers = { "Accept" => "application/json", "Content-Type" => "application/json" }
      Devise::JWT::TestHelpers.auth_headers(headers, user)
    end
  end

  def json_response
    @json_response ||= JSON.parse(response.body)
  end
end
