# frozen_string_literal: true

require "factory_bot_rails"

module FactoryBotHelper
  RSpec.configure do |config|
    config.include FactoryBot::Syntax::Methods
  end
end
