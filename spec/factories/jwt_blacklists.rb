# frozen_string_literal: true

FactoryBot.define do
  factory :jwt_blacklist do
    jti { "MyString" }
    exp { "2020-03-29 14:52:58" }
  end
end
