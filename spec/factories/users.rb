# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email                 { FFaker::Internet.unique.email }
    password              { "12345678" }
    password_confirmation { password }
    role                  { :admin }
  end
end
