FactoryBot.define do
  factory :task do
    title { "MyString" }
    description { "MyText" }
    user { nil }
    product { nil }
  end
end
