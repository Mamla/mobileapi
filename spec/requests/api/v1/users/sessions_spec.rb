# frozen_string_literal: true

RSpec.describe "Api::V1::Users::Sessions", type: :request do
  let(:user) { create(:user) }
  let(:type) { "admin" }

  describe "login#create" do
    let(:params) do
      {
        user: {
          email: user.email,
          password: user.password
        }
      }
    end

    before { post new_user_session_path, params: params }

    context "when params are correct" do
      shared_examples "log in user" do
        it_behaves_like "returns 200 HTTP status"

        it "returns JWT token in authorization header" do
          binding.pry
          expect(response.header["Authorization"]).to be_present
        end

        it "returns valid JWT token" do
          token_from_request = response.headers["Authorization"].split(" ").last
          decoded_token = JWT.decode(token_from_request, ENV["DEVISE_JWT_SECRET_KEY"], true)
          expect(decoded_token.first["sub"]).to be_present
        end

        it "returns participant data" do
          expect(json_response["data"]).to be_present
          expect(json_response["data"]).to have_id(user.id.to_s)
          expect(json_response["data"]).to have_type(type)
          expect(json_response["data"]).to have_attribute(:email).with_value(user.email)
          expect(json_response["data"]).to have_attribute(:role).with_value(type)
        end
      end

      context "as an admin user" do
        let(:user) { create(:user) }
        let(:type) { "admin" }

        it_behaves_like "log in user"
      end

      context "as a participant user" do
        it_behaves_like "log in user"
      end
    end

    context "when no params passed" do
      let(:params) { }

      it_behaves_like "returns 401 HTTP status"

      it "returns error message" do
        expect(json_response["errors"]).to be_present
        expect(json_response["errors"]).to eq("You need to sign in or sign up before continuing.")
      end
    end

    context "when params are incorrect" do
      let(:params) do
        {
          user: {
            email: "dummy@email.com",
            password: user.password
          }
        }
      end

      it_behaves_like "returns 401 HTTP status"

      it "returns error message" do
        expect(json_response["errors"]).to be_present
        expect(json_response["errors"]).to eq("Invalid Email or password.")
      end
    end
  end

  describe "logout#destroy" do
    include_context "API v1 authorization token"

    let(:execute) { delete destroy_user_session_path, headers: headers_bearer }

    context "when authorization token is provided" do
      shared_examples "log out user" do
        context "and request is performed" do
          it_behaves_like "returns 200 HTTP status"
        end

        it "token is revoked" do
          expect { execute }.to change { JwtBlacklist.count }.by(1)
        end
      end

      context "for an admin user" do
        let(:user) { create(:user) }
        it_behaves_like "log out user"
      end

      context "for an participant user" do
        it_behaves_like "log out user"
      end
    end

    context "when authorization token is not provided" do
      let(:headers_bearer) { }

      context "and request is performed" do
        before { execute }
        it_behaves_like "returns 200 HTTP status"
      end

      it "token is not revoked" do
        expect { execute }.not_to change { JwtBlacklist.count }
      end
    end
  end
end
