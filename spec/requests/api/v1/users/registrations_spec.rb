# frozen_string_literal: true

RSpec.describe "Api::V1::Users::Registrations", type: :request do
  describe "signup#create" do
    let(:execute) { post user_registration_path, params: params }
    let(:email) { FFaker::Internet.unique.email }
    let(:params) do
      {
        user: {
          email: email,
          password: "12345678",
          password_confirmation: "12345678"
        }
      }
    end

    context "when participant does not exist" do
      it_behaves_like "returns 201 HTTP status"

      it "returns expected response" do
        execute
        expect(json_response["data"]).to be_present
        expect(json_response["data"]).to have_id(User.last.id.to_s)
        expect(json_response["data"]).to have_type("client")
        expect(json_response["data"]).to have_attribute(:email).with_value(email)
        expect(json_response["data"]).to have_attribute(:role).with_value("participant")
      end

      it "creates a new participant account" do
        expect { execute }.to change { User.count }.by(1)
      end
    end

    context "when participant already exists" do
      before do
        create(:user, email: params[:user][:email])
      end

      it_behaves_like "returns 422 HTTP status"

      it "returns validation errors" do
        execute
        expect(json_response["errors"]).to be_present
        expect(json_response["errors"]["email"]).to eq(["has already been taken"])
      end
    end
  end
end
