# frozen_string_literal: true

class Api::V1::Users::SessionsController < Devise::SessionsController
  respond_to :json

  private
    def respond_with(resource, _opts = {})
      if resource.errors.present?
        render_error_payload(resource.errors)
      else
        render json: UserSerializer.new(resource)
      end
    end

    def respond_to_on_destroy
      head :ok
    end
end
