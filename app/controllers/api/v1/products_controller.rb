class Api::V1::ProductsController < ApplicationController
  before_action :set_product, only: [:update, :destroy]

  def index
    render json: serializer.new(Product.all), status: :ok
  end

  def create
    product = Product.new(product_params)
    if product.save
      render json: serializer.new(product), status: :created
    else
      render_error_payload(product.errors)
    end
  end

  def update
    if @product.update_attributes(product_params)
      render json: serializer.new(@product), status: :ok
    else
      render_error_payload(@product.errors)
    end
  end

  def destroy
    @product.delete
    head :no_content
  end

   private
    def product_params
      params.require(:product).permit(:title)
    end

    def set_product
      @product = Product.find(params[:id])
    end

    def serializer
      ProductSerializer
    end
end
