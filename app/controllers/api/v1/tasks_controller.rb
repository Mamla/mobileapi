class Api::V1::TasksController < ApplicationController
  before_action :set_task, only: [:update, :destroy]

  def index
    render json: serializer.new(Task.all), status: :ok
  end

  def create
    task = Task.new(task_params)
    if task.save
      render json: serializer.new(task), status: :created
    else
      render_error_payload(task.errors)
    end
  end

  def update
    if @task.update_attributes(task_params)
      render json: serializer.new(@task), status: :ok
    else
      render_error_payload(@task.errors)
    end
  end

  def destroy
    @task.delete
    head :no_content
  end

   private
    def task_params
      params.require(:task).permit(:title, :description, :user_id, :product_id)
    end

    def set_task
      @task = Task.find(params[:id])
    end

    def serializer
      TaskSerializer
    end
end
