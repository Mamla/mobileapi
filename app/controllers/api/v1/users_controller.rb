class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:update, :destroy]

  def index
    render json: serializer.new(User.all), status: :ok
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: serializer.new(user), status: :created
    else
      render_error_payload(user.errors)
    end
  end

  def update
    if @user.update_attributes(user_params)
      render json: serializer.new(@user), status: :ok
    else
      render_error_payload(@user.errors)
    end
  end

  def destroy
    @user.delete
    head :no_content
  end

   private
    def user_params
      params.require(:user).permit(:email, :name, :role, :password, :password_confirmation)
    end

    def set_user
      @user = User.find(params[:id])
    end

    def serializer
      UserSerializer
    end
end
