# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Pagy::Backend
  include Pundit

  before_action :authenticate_user!

  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def route_not_found
    not_found
  end

  private
    def not_found
      errors = ["The resource you were looking for could not be found."]
      render_error_payload(errors, :not_found)
    end

    def user_not_authorized
      error = ["You are not authorized to perform this action."]
      render_error_payload(error, 403)
    end

    def render_error_payload(errors, status = :unprocessable_entity)
      render(json: { errors: errors }, status: status) && (return)
    end

    def options(pagy)
      pagy_meta = pagy_metadata(pagy)
      { include: resource_includes, links: generate_pagination(pagy_meta), meta: generate_meta(pagy_meta) }
    end

    def generate_pagination(pagy_meta)
      url = request.base_url
      {
        first: url + pagy_meta[:first_url],
        prev: url + pagy_meta[:prev_url],
        next: url + pagy_meta[:next_url],
        last: url + pagy_meta[:last_url]
      }
    end

    def generate_meta(pagy_meta)
      {
        current_page: pagy_meta[:page], total_pages: pagy_meta[:pages]
      }
    end
end
