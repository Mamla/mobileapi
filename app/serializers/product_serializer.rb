# frozen_string_literal: true

class ProductSerializer < BaseSerializer
  set_type :product

  attributes :id, :title
end
