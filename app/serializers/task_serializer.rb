# frozen_string_literal: true

class TaskSerializer < BaseSerializer
  set_type :task

  attributes :id, :title, :description
end
