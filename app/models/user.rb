# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :jwt_authenticatable, jwt_revocation_strategy: JwtBlacklist

  validates :password_confirmation, presence: true, if: :password_required?
  validates :role, presence: true

  enum role: { client: 0, employee: 1, admin: 2 }
  has_many :tasks
end
