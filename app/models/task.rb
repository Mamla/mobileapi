class Task < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validates :title, presence: true
  validates :description, presence: true
end
